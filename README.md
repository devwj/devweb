Nos arquivos em questão, 
        
- [ ]  A pasta "IntroHTML" contém as pastas HTML 0 e HTML 1, cada um com o módulo específico do exercício de HTML
- [ ] A pasta "IntroJS" contém um módulo um pouco mais avançado, referente a JavaScript, foi um exercício extra que eu commitei.
- [ ] As pastas, JS1, JS2 e JS3 são os 3 módulos JavaScript, em que cada pasta contém seus respectivos exercícios.
