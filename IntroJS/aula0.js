/*Você pode executar os comandos em javascript no próprio chrome ou em um editor de código,
  tal como o VScode*/
  //Chrome:
    // 1: Abra o navegador Chrome;
    // 2: aperte F12 para acessar o console;
    // 3: algumas mensagens de erro poderão aparecer no console, mas não se preocupe;
    // 4: você está pronto para codar.
  //VScode:
    // 1: instale o VScode pelo link: https://code.visualstudio.com/;
    // 2: depois de instalado, abra o VScode e selecione uma pasta do seu computador para guardar o projeto;  
    // 3: vá em extensões e instale a extensão 'Node.js Exec', para rodar seus códigos no console apertando apenas F8;
    // 4: você está pronto para codar;

// operações
    //var é a inicialização de uma variável;
    //console.log() é uma maneira de mostrar algo prompt;
n1=16
n2=8
console.log(n1+n2) //soma
console.log(n1-n2) //subtração
console.log(n1*n2) //multiplicação
console.log(n1/n2) //divisão


console.log('-----------------------------------------------------')
// variáveis
    //é correto colocar o termo 'var' sempre que for inicializar uma variável;
    //elas podem receber valores inteiros, de ponto flutuante, strings e booleanos;
var num=10
var name='João augusto'
var numFloat=1.5
var aderbalTop=true
    //Se você rodar no chrome o resultado vai ser undefined, pois todas as variáveis criadas começam como undefined;
    //Você pode colocar um console.log para ver o resultado esperado;
console.log(num, name, numFloat, aderbalTop)


console.log('-----------------------------------------------------')
// alert
    //vamos criar seu primeiro alert;
    /*infelizmente, se você estiver no VScode, essa parte só será possível com a criação de um arquivo html,
     para conseguir rodar na web então, nesta etapa, realize esta tarefa no console do chrome;*/
//alert('Olá meuamigo')


console.log('-----------------------------------------------------')
// Array
    //se você quiser trabalhar com vários valores armazenados, use o array
var suasNotas = ['SS', 'MS', 'SS', 'SS'] //Sim, você é um bom aluno, acredite no seu potencial <3;

    //adicionando valores na lista com push();
suasNotas.push('MM') //xiiii, um MM?!
console.log(suasNotas)

    //removendo itens da lista com pop();
suasNotas.pop() //brincadeira, você não tiraria um MM, NÃO É?!
console.log(suasNotas)
    //o pop() remove o último elemnto de uma lista, funcionando como uma pilha;


console.log('-----------------------------------------------------')
// Estrutura de laço: For
    //não gostou muito de como a lista foi mostrada no console?
    //experimente essa estrutura:
for (let i=0; i < suasNotas.length; i++){
    console.log(`Nota ${i+1}: ${suasNotas[i]}`)
}
    /*o for não é somente para isso, claro, ele é usado quando se quer executar um trecho de código
      até que uma determinada condição seja estabelecida*/


console.log('-----------------------------------------------------')

//Acho que já podemos ir para a parte de JS + HTML, não acham?!
//Então vamos criar um novo arquivo